# task-manager

## developer

**Denis Kozyaikin**

**dkozyaikin@t1-consulting.ru**

## description

**JAVA/Spring development course project**

## software

- **Java**: OpenJDK 17
- **OS**: macOS Big Sur

## hardware

- **CPU**: Intel Core i7 2,3 GHz
- **RAM**: 16Gb DDR4 3733 MHz

## build application

`mvn clean install`

## run application

```
run app: `java -jar ./target/task-manager-1.5.0.jar`
Display app commands: `java -jar ./target/task-manager-1.5.0.jar -h`
Display app version: `java -jar ./target/task-manager-1.5.0.jar -v`
Display developer info: `java -jar ./target/task-manager-1.5.0.jar -a`
```
