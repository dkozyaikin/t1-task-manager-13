package ru.t1.dkozyaikin.tm.api.controller;

public interface ICommandController {

    void displayWelcome();

    void displayErrorCommand();

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayInfo();

    void displayArguments();

    void displayCommands();

    void clearOutput();

}
