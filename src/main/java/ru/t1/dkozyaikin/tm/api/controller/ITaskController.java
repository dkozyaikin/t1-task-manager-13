package ru.t1.dkozyaikin.tm.api.controller;

import ru.t1.dkozyaikin.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void displayTask(Task task);

    void displayTaskById();

    void displayTaskByIndex();

    void displayTaskByProjectId();

    void displayTasks();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
